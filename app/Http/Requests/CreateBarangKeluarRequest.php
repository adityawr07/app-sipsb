<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateBarangKeluarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'tanggal_transaksi' => 'required',
            'keterangan' => 'nullable',
            'nama_pelanggan' => 'required',
            'alamat_pelanggan' => 'nullable',
            'kontak_pelanggan' => 'nullable',
            'nama_pengirim' => 'nullable',
            'tanggal_pengiriman' => 'required',
            'jumlah_pembayaran' => 'required',
            'tanggal_pembayaran' => 'required',
            'items' => 'required|array|bail',
            'items.*.barang_id' => 'required',
            'items.*.qty' => 'required|min:1',
        ];
    }
    
    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors()->first();
        throw new HttpResponseException(
            response()->json($errors, 422)
        );
    }
}

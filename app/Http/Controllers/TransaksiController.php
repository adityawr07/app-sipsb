<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBarangMasukRequest;
use App\Http\Requests\CreateBarangKeluarRequest;
use App\Models\StokBarang;
use App\Models\TransaksiBarangKeluar;
use App\Models\TransaksiBarangMasuk;
use App\Services\StokService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\JsonResponse;

class TransaksiController extends Controller
{
    public function barangMasuk(CreateBarangMasukRequest $request, StokService $stokService)
    {
        return DB::transaction(function () use ($request, $stokService) {
            if (array_key_exists('id', $request->all()) && isset($request->id)) {
                $barangMasuk = TransaksiBarangMasuk::find($request->id);
                $barangMasuk->update(Arr::except($request->validated(), 'items'));
                $itemsId = [];
                foreach ($request->validated('items') as $key => $value) {
                    if (array_key_exists('id', $value) && isset($value['id'])) {
                        $barangMasukDetail = $barangMasuk->items()->find($value['id']);
                        $qtyLama = $barangMasukDetail->qty;
                        $qtyBaru = $value['qty'];
                        if ($qtyBaru != $qtyLama) {
                            $stok = StokBarang::where([
                                'barang_id' => $barangMasukDetail->barang_id
                            ])->first();
                            $stok->decrement('stok', $qtyLama);
                            $stok->increment('stok', $qtyBaru);
                        }
                        $barangMasukDetail->update($value);
                        $itemsId[] = $value['id'];
                    } else {
                        $barangMasukDetail = $barangMasuk->items()->create($value);
                        $stokService->increment($barangMasukDetail->barang_id, $barangMasukDetail->qty);
                        $itemsId[] = $barangMasukDetail->id;
                    }
                }
                $barangMasuk->items()->whereNotIn('id', $itemsId)->delete();
            } else {
                $barangMasuk = TransaksiBarangMasuk::create(Arr::except($request->validated(), ['items']));
                if (array_key_exists('items', $request->validated()) && !empty($request->validated('items'))) {
                    foreach ($request->validated('items') as $key => $value) {
                        $barangMasukDetail = $barangMasuk->items()->create($value);
                        $stokService->increment($barangMasukDetail->barang_id, $barangMasukDetail->qty);
                    }
                }
            }
            return response()->json($barangMasuk->load('items.barang'));
        });
    }

    public function riwayatTransaksiBarangMasuk()
    {
        $result = TransaksiBarangMasuk::query()
            ->whereYear('tanggal_transaksi', date('Y', strtotime(now())))
            ->whereMonth('tanggal_transaksi', date('m', strtotime(now())))
            ->with('items.barang')
            ->latest()
            ->get();
        return response()->json($result);
    }

    public function deleteTransaksiBarangMasuk(Request $request)
    {
        return DB::transaction(function() use($request) {
            $barangMasuk = TransaksiBarangMasuk::find($request->id);
            foreach ($barangMasuk->items as $key => $value) {
                $stok = StokBarang::where([
                    'barang_id' => $value->barang_id
                ])->first();
                $stok->decrement('stok', $value->qty);
                $value->delete();
            }
            $barangMasuk->delete();
            return;
        });
    }

    //barang keluar

    public function barangKeluar(CreateBarangKeluarRequest $request, StokService $stokService)
    {
        return DB::transaction(function () use ($request, $stokService) {
            if (array_key_exists('id', $request->all()) && isset($request->id)) {
                $barangKeluar = TransaksiBarangKeluar::find($request->id);
                $barangKeluar->update(Arr::except($request->validated(), 'items'));
                $itemsId = [];
                foreach ($request->validated('items') as $key => $value) {
                    if (array_key_exists('id', $value) && isset($value['id'])) {
                        $barangKeluarDetail = $barangKeluar->items()->find($value['id']);
                        $qtyLama = $barangKeluarDetail->qty;
                        $qtyBaru = $value['qty'];
                        if ($qtyBaru != $qtyLama) {
                            $stok = StokBarang::where([
                                'barang_id' => $barangKeluarDetail->barang_id
                            ])->first();
                            $stok->increment('stok', $qtyLama);
                            $stok->decrement('stok', $qtyBaru);
                        }
                        $barangKeluarDetail->update($value);
                        $itemsId[] = $value['id'];
                    } else {
                        $barangKeluarDetail = $barangKeluar->items()->create($value);
                        $stokService->decrement($barangKeluarDetail->barang_id, $barangKeluarDetail->qty);
                        $itemsId[] = $barangKeluarDetail->id;
                    }
                }
                $barangKeluar->items()->whereNotIn('id', $itemsId)->delete();
            } else {
                $barangKeluar = TransaksiBarangKeluar::create(Arr::except($request->validated(), ['items']));
                if (array_key_exists('items', $request->validated()) && !empty($request->validated('items'))) {
                    foreach ($request->validated('items') as $key => $value) {
                        $barangKeluarDetail = $barangKeluar->items()->create($value);
                        $stokService->decrement($barangKeluarDetail->barang_id, $barangKeluarDetail->qty);
                    }
                }
            }
            return response()->json($barangKeluar->load('items.barang'));
        });
    }

    public function riwayatTransaksiBarangKeluar()
    {
        $result = TransaksiBarangKeluar::query()
            ->whereYear('tanggal_transaksi', date('Y', strtotime(now())))
            ->whereMonth('tanggal_transaksi', date('m', strtotime(now())))
            ->with('items.barang')
            ->latest()
            ->get();
        return response()->json($result);
    }

    public function deleteTransaksiBarangKeluar(Request $request)
    {
        return DB::transaction(function() use($request) {
            $barangKeluar = TransaksiBarangKeluar::find($request->id);
            foreach ($barangKeluar->items as $key => $value) {
                $stok = StokBarang::where([
                    'barang_id' => $value->barang_id
                ])->first();
                $stok->increment('stok', $value->qty);
                $value->delete();
            }
            $barangKeluar->delete();
            return;
        });
    }
}

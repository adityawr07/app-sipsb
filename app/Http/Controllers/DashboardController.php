<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Berita;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index()
    {
        $data = Berita::all();
        $komposisi = \DB::table('barang')
                // ->join('m_pasien', 'm_pasien.id', '=', 'v_kunjungan_pasien.pasien_id')
                ->select([
                    \DB::raw('count(*) as jumlah'),
                    \DB::raw('kelompok_barang as data'),
                    // \DB::raw('kode_icd10 as kode'),
                ])
                // ->whereYear('tanggal_registrasi' , date('Y'))
                ->groupBy('data')
                ->orderBy('jumlah', 'desc')
                // ->limit(10)
                // ->pluck('jumlah', 'kode')
                ->get();
        // dd($komposisi);
        $total = Barang::sum('satuan_barang');
        $date = date('Y');
        // dd($total);
        return view('dashboard/index ', compact('data','total','date'));
    }

    public function berita()
    {
        $berita = Berita::all();
        return view('dashboard/berita',compact('berita'));
    }
    public function visimisi()
    {
        return view('dashboard/visi-misi');
    }
    public function timbunansampah()
    {
        $timbunan = Barang::with('kelurahan')->get();
        return view('dashboard/timbunan-sampah',compact('timbunan'));
    }
    public function komposisisampah()
    {
        $komposisi = Barang::with('kelurahan')->get();
        return view('dashboard/komposisi-sampah',compact('komposisi'));
    }
    public function sumbersampah()
    {
        $sumber = Barang::with('kelurahan')->get();
        return view('dashboard/sumber-sampah',compact('sumber'));
    }
    public function banksampah()
    {
        return view('dashboard/bank-sampah');
    }
    public function rumahkompos()
    {
        return view('dashboard/rumah-kompos');
    }
    public function tps3r()
    {
        return view('dashboard/TPS3R');
    }
    public function kontak()
    {
        return view('dashboard/contact');
    }
}

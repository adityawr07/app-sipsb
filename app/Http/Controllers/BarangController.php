<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Supplier;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    public function getBarang(Barang $barang)
    {
        return response()->json($barang);
    }

    public function getSupplier(Supplier $supplier)
    {
        return response()->json($supplier);
    }

    public function listBarang(Request $request)
    {
        $barang = Barang::query()
            ->when($request->filled('keyword'), fn ($q) => $q->where('nama_barang', 'LIKE', '%' . $request->keyword . '%'))
            ->get();

        return response()->json($barang);
    }
}

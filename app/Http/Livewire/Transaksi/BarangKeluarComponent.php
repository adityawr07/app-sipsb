<?php

namespace App\Http\Livewire\Transaksi;

use Livewire\Component;

class BarangKeluarComponent extends Component
{
    public function render()
    {
        return view('livewire.transaksi.barang-keluar-component');
    }
}

<?php

namespace App\Http\Livewire\Transaksi;

use App\Models\Barang;
use App\Models\Supplier;
use Livewire\Component;

class BarangMasukComponent extends Component
{
    public function getSupplier($id) 
    {
        return json_encode(Supplier::find($id));
    }

    public function getBarang($id) 
    {
        return json_encode(Barang::find($id));
    }

    public function render()
    {
        return view('livewire.transaksi.barang-masuk-component')->layout('layouts.app');
    }
}

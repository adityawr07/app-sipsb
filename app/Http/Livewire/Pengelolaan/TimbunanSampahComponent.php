<?php

namespace App\Http\Livewire\Pengelolaan;

use App\Models\Barang;
use Livewire\Component;

class TimbunanSampahComponent extends Component
{
    // public $label_data_bulan   = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];
    public $loaded = true;
    public function render()
    {
        $data_pasien_rj  = [];
        $label_pasien_rj = [];
        $poli            = [];

        if ($this->loaded) {
            $max_hari = date('m');
            // $label_data_bulan   = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];
            for ($i = 1; $i <= $max_hari; $i++) {
                $data_pasien_rj[] = rand(6, 10);
                $label_pasien_rj[] = $i;
            }

            $this->emit("render_chart_kunjungan_pasien_rj", $data_pasien_rj, $label_pasien_rj);
        }
        $timbunan = Barang::with('kelurahan')->get();
        return view('livewire.pengelolaan.timbunan-sampah-component',compact('timbunan'));
        // return view('dashboard/timbunan-sampah',compact('timbunan'));
    }
}

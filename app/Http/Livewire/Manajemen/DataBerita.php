<?php

namespace App\Http\Livewire\Manajemen;

use Livewire\Component;
use App\Models\Berita;
use Livewire\WithFileUploads;

class DataBerita extends Component
{
    use WithFileUploads;

    public $berita;
    public $form;
    public $stateForm = false;

    protected $rules = [
        'form.judul_berita' => 'required',
        'form.isi_berita' => 'required',
        'form.foto_berita' => 'required',
    ];

    protected $listeners = ['deleteSupplier'];

    public function mount()
    {
        $this->berita = Berita::all();
    }

    public function cancelForm()
    {
        $this->clearForm();
        $this->stateForm = false;
        $this->mount();
    }

    public function clearForm()
    {
        $this->form['id'] = '';
        $this->form['judul_berita'] = '';
        $this->form['isi_berita'] = '';
        $this->form['foto_berita'] = '';
    }

    public function addData()
    {
        $this->stateForm = true;
    }

    public function editData(Berita $berita)
    {

        $this->stateForm = true;
        $this->form['id'] = $berita->id;
        $this->form['judul_berita'] = $berita->judul_berita;
        $this->form['isi_berita'] = $berita->isi_berita;
        $this->form['foto_berita'] = $berita->foto_berita;
    }

    public function submit()
    {

        if (isset($this->form['foto_berita'])) {

            $url_foto_berita = $this->form['foto_berita']->store('berkas', 'public');
            $this->form['foto_berita'] = $url_foto_berita;
        }


        try {
            $this->validate();
            if (array_key_exists('id', $this->form) && !empty($this->form['id'])) {
                $berita = Berita::find($this->form['id']);
                $berita->update($this->form);
            } else {
                $berita = Berita::create([
                    'judul_berita' => $this->form['judul_berita'],
                    'isi_berita' => $this->form['isi_berita'],
                    'foto_berita' => $this->form['foto_berita'],
                ]);
            }
            $this->clearForm();
            $this->emit('alert-success', 'Berhasil menyimpan');
        } catch (\Throwable $th) {
            // dd($th->getMessage(), $th->getLine());
            $this->emit('alert-error', $th->getMessage());
        }
    }

    // public function deleteBarang(Berita $berita)
    // {

    //     try {
    //         $berita->delete();
    //         $this->mount();
    //     } catch (\Throwable $th) {
    //         dd($th->getMessage(), $th->getLine());
    //         $this->emit('alert-error', $th->getMessage());
    //     }
    // }

    public function deleteBerita(Berita $berita)
    {
        $berita->delete();
        $this->mount();
    }



    public function render()
    {
        return view('livewire.manajemen.data-berita');
    }
}

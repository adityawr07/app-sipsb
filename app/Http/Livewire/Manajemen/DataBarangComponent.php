<?php

namespace App\Http\Livewire\Manajemen;

use App\Models\Barang;
use App\Models\Kelurahan;
use App\Models\StokBarang;
use Livewire\Component;

class DataBarangComponent extends Component
{
    public $barang;
    public $form;
    public $stateForm = false;

    protected $rules = [
        'form.satuan_barang' => '',
        'form.kelompok_barang' => '',
        'form.sumber_barang' => '',
        'form.kecamatan' => '',
        'form.kelurahan_id' => '',
        'form.rw' => 'integer',
        'form.rt' => 'integer',
    ];

    protected $listeners = ['deleteBarang'];

    public function mount()
    {
        $this->barang = Barang::all();
        $this->form['kecamatan'] = "Semarang Barat";
    }

    public function cancelForm()
    {
        $this->clearForm();
        $this->stateForm = false;
        $this->mount();
    }

    public function clearForm()
    {
        $this->form['id'] = '';
        $this->form['kode_barang'] = '';
        $this->form['satuan_barang'] = '';
        $this->form['kelompok_barang'] = '';
        $this->form['sumber_barang'] = '';
        $this->form['kecamatan'] = '';
        $this->form['kelurahan_id'] = '';
        $this->form['rw'] = '';
        $this->form['rt'] = '';
    }

    public function addBarang()
    {
        $this->stateForm = true;
    }

    public function editBarang(Barang $barang)
    {
        $this->stateForm = true;
        $this->form['id'] = $barang->id;
        $this->form['kode_barang'] = $barang->kode_barang;
        $this->form['satuan_barang'] = $barang->satuan_barang;
        $this->form['kelompok_barang'] = $barang->kelompok_barang;
        $this->form['sumber_barang'] = $barang->sumber_barang;
        $this->form['kecamatan'] =  $barang->kecamatan;
        $this->form['kelurahan_id'] =  $barang->kelurahan_id;
        $this->form['rw'] =  $barang->rw;
        $this->form['rt'] =  $barang->rt;
    }

    public function submit()
    {
        try {
            $this->validate();
            if (array_key_exists('id', $this->form) && !empty($this->form['id'])) {
                $barang = Barang::find($this->form['id']);
                $barang->update($this->form);
            } else {
                $barang = Barang::create([
                    'satuan_barang'     => $this->form['satuan_barang'],
                    'kelompok_barang'   => $this->form['kelompok_barang'],
                    'sumber_barang'     => $this->form['sumber_barang'],
                    'kecamatan'         => $this->form['kecamatan'],
                    'kelurahan_id'      => $this->form['kelurahan_id'],
                    'rw'                => $this->form['rw'],
                    'rt'                => $this->form['rt'],
                ]);
            }
            $this->clearForm();
            $this->emit('alert-success', 'Berhasil menyimpan');
        } catch (\Throwable $th) {
            // dd($th->getMessage(), $th->getLine());
            $this->emit('alert-error', $th->getMessage());
        }
    }

    public function deleteBarang(Barang $barang)
    {
        try {
            // $stok->delete();
            $barang->delete();
            $this->mount();
        } catch (\Throwable $th) {
            // dd($th->getMessage(), $th->getLine());
            $this->emit('alert-error', $th->getMessage());
        }
    }

    public function render()
    {
        $kelurahan = Kelurahan::pluck('nama','id')->toArray();
        return view('livewire.manajemen.data-barang-component',[
            'kelurahan' => $kelurahan,

        ]);
    }
}

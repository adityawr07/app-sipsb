<?php

namespace App\Http\Livewire\Manajemen;

use App\Models\Supplier;
use Livewire\Component;

class DataSupplierComponent extends Component
{
    public $supplier;
    public $form;
    public $stateForm = false;

    protected $rules = [
        'form.nama_supplier' => '',
        'form.email' => '',
        'form.alamat' => '',
        'form.telepon' => '',
    ];

    protected $listeners = ['deleteSupplier'];

    public function mount()
    {
        $this->supplier = Supplier::all();
    }
    
    public function cancelForm()
    {
        $this->clearForm();
        $this->stateForm = false;
        $this->mount();
    }

    public function clearForm()
    {
        $this->form['id'] = '';
        $this->form['nama_supplier'] = '';
        $this->form['email'] = '';
        $this->form['alamat'] = '';
        $this->form['telepon'] = '';
    }

    public function addSupplier()
    {
        $this->stateForm = true;
    }
    
    public function editSupplier(Supplier $supplier)
    {
        $this->stateForm = true;
        $this->form['id'] = $supplier->id;
        $this->form['nama_supplier'] = $supplier->nama_supplier;
        $this->form['email'] = $supplier->email;
        $this->form['alamat'] = $supplier->alamat;
        $this->form['telepon'] = $supplier->telepon;
    }

    public function submit()
    {
        try {
            $this->validate();
            if (array_key_exists('id', $this->form) && !empty($this->form['id'])) {
                $supplier = Supplier::find($this->form['id']);
                $supplier->update($this->form);
            } else {
                $supplier = Supplier::create([
                    'nama_supplier' => $this->form['nama_supplier'],
                    'email' => $this->form['email'],
                    'alamat' => $this->form['alamat'],
                    'telepon' => $this->form['telepon'],
                ]);
            }
            $this->clearForm();
            $this->emit('alert-success', 'Berhasil menyimpan');
        } catch (\Throwable $th) {
            dd($th->getMessage(), $th->getLine());
            $this->emit('alert-error', $th->getMessage());
        }
    }

    public function deleteSupplier(Supplier $supplier)
    {
        try {
            $supplier->delete();
            $this->mount();
        } catch (\Throwable $th) {
            dd($th->getMessage(), $th->getLine());
            $this->emit('alert-error', $th->getMessage());
        }        
    }

    public function render()
    {
        return view('livewire.manajemen.data-supplier-component');
    }
}

<?php

namespace App\Http\Livewire\Laporan;

use App\Models\TransaksiBarangKeluar;
use Livewire\Component;

class BarangKeluarComponent extends Component
{
    public $no_transaksi;
    public $bulan;
    public $tahun;
    public $transaksi;

    public function mount()
    {
        $this->bulan = date('n');
        $this->tahun = date('Y');
        $this->transaksi = TransaksiBarangKeluar::all();
    }
    
    public function search()
    {
        $this->transaksi = TransaksiBarangKeluar::query()
        ->when($this->no_transaksi, fn ($query) => $query->where('no_transaksi', 'LIKE', '%'.$this->no_transaksi.'%'))
        ->whereMonth('tanggal_transaksi', $this->bulan)
        ->whereYear('tanggal_transaksi', $this->tahun)
        ->get();
    }

    public function render()
    {
        return view('livewire.laporan.barang-keluar-component');
    }
}

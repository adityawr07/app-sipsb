<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TransaksiBarangMasuk extends Model
{
    use HasFactory;
    protected $table = 'transaksi_barang_masuk';
    protected $guarded = [];

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->no_transaksi = getNoTransaksiBarangMasuk($model->tanggal_transaksi);
        });
    }

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }

    public function items(): HasMany
    {
        return $this->hasMany(TransaksiBarangMasukDetail::class, 'barang_masuk_id');
    }
}

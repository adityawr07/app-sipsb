<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TransaksiBarangKeluar extends Model
{
    use HasFactory;
    protected $table = 'transaksi_barang_keluar';
    protected $guarded = [];

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->no_transaksi = getNoTransaksiBarangKeluar($model->tanggal_transaksi);
        });
    }

    public function items(): HasMany
    {
        return $this->hasMany(TransaksiBarangKeluarDetail::class, 'barang_keluar_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Barang extends Model
{
    use HasFactory;
    protected $table = 'barang';
    protected $guarded = [];

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->kode_barang = getKodeBarang();
        });
    }

    public function stok(): HasOne
    {
        return $this->hasOne(StokBarang::class);
    }

    public function kelurahan(): BelongsTo
    {
        return $this->BelongsTo(Kelurahan::class, 'kelurahan_id');
    }
}

<?php

namespace App\Services;

use App\Models\StokBarang;

class StokService 
{
    public function increment(int $barangId, int $qty)
    {
        $stok = StokBarang::where([
            'barang_id' => $barangId
        ])->first();

        throw_if(is_null($stok), "Stok barang tidak ditemukan! Barang #{$barangId}");

        $stok->increment('stok', $qty);

        return $stok;
    }

    public function decrement(int $barangId, int $qty)
    {
        $stok = StokBarang::where([
            'barang_id' => $barangId
        ])->first();

        throw_if(is_null($stok), "Stok barang tidak ditemukan! Barang #{$barangId}");

        $stok->decrement('stok', $qty);

        return $stok;
    }
}
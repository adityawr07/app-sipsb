<?php

use App\Models\Barang;
use App\Models\Supplier;
use App\Models\TransaksiBarangKeluar;
use App\Models\TransaksiBarangMasuk;

if (!function_exists('getSupplier')) {
    function getSupplier()
    {
        return Supplier::pluck('nama_supplier', 'id')->toArray();
    }
}

if (!function_exists('getBarang')) {
    function getBarang()
    {
        return Barang::pluck('nama_barang', 'id')->toArray();
    }
}

if (!function_exists('getNoTransaksiBarangMasuk')) {
    function getNoTransaksiBarangMasuk($tanggal_transaksi): string
    {
        $no = 1;
        $prefix = date('Ymd', strtotime($tanggal_transaksi));

        $latestOrder = TransaksiBarangMasuk::query()
            ->whereYear('tanggal_transaksi', date('Y', strtotime($tanggal_transaksi)))
            ->whereMonth('tanggal_transaksi', date('m', strtotime($tanggal_transaksi)))
            ->latest()
            ->first();

        if ($latestOrder) {
            $no = ((int) substr($latestOrder->no_transaksi, 8)) + 1;
        }

        $infix = str_pad($no, 8, '0', STR_PAD_LEFT);

        return $prefix . $infix;
    }
}

if (!function_exists('getNoTransaksiBarangKeluar')) {
    function getNoTransaksiBarangKeluar($tanggal_transaksi): string
    {
        $no = 1;
        $prefix = date('Ymd', strtotime($tanggal_transaksi));

        $latestOrder = TransaksiBarangKeluar::query()
            ->whereYear('tanggal_transaksi', date('Y', strtotime($tanggal_transaksi)))
            ->whereMonth('tanggal_transaksi', date('m', strtotime($tanggal_transaksi)))
            ->latest()
            ->first();

        if ($latestOrder) {
            $no = ((int) substr($latestOrder->no_transaksi, 8)) + 1;
        }

        $infix = str_pad($no, 8, '0', STR_PAD_LEFT);

        return $prefix . $infix;
    }
}

if (!function_exists('getKodeBarang')) {
    function getKodeBarang(): string
    {
        $no = 1;
        $prefix = date('Ymd', strtotime(now()));

        $latestCode = Barang::query()
            ->whereYear('created_at', date('Y', strtotime(now())))
            ->whereMonth('created_at', date('m', strtotime(now())))
            ->latest()
            ->first();

        if ($latestCode) {
            $no = ((int) substr($latestCode->kode_barang, 8)) + 1;
        }

        $infix = str_pad($no, 8, '0', STR_PAD_LEFT);

        return $prefix . $infix;
    }
}

if (!function_exists('getMonth')) {
    function getMonth()
    {
        return [
            ['id' => 1, 'bulan' => 'Januari'],
            ['id' => 2, 'bulan' => 'Februari'],
            ['id' => 3, 'bulan' => 'Maret'],
            ['id' => 4, 'bulan' => 'April'],
            ['id' => 5, 'bulan' => 'Mei'],
            ['id' => 6, 'bulan' => 'Juni'],
            ['id' => 7, 'bulan' => 'Juli'],
            ['id' => 8, 'bulan' => 'Agustus'],
            ['id' => 9, 'bulan' => 'September'],
            ['id' => 10, 'bulan' => 'Oktober'],
            ['id' => 11, 'bulan' => 'November'],
            ['id' => 12, 'bulan' => 'Desember'],
        ];
    }
}
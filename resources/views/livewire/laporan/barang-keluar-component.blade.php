<div>
    <x-slot:title>Laporan Barang Keluar</x-slot:title>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body p-2">
                    <div class="row">
                        <div class="col-6">
                            <form wire:submit.prevent="search">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">No. Transaksi</label>
                                        <input type="text" class="form-control form-control-sm" style="font-size: 10px; height: 31px" wire:model.defer="no_transaksi">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">Bulan</label>
                                        <select wire:model.defer="bulan" class="form-control form-control-sm p-0" style="
                                            font-size: 10px;
                                            height: 31px;
                                        ">
                                            @foreach (getMonth() as $item)
                                                <option value="{{ $item['id'] }}">{{ $item['bulan'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="tahun">Tahun</label>
                                        <select wire:model.defer="tahun" class="form-control form-control-sm p-0" style="
                                            font-size: 10px;
                                            height: 31px;
                                        ">
                                            @for ($i = 2020; $i <= date('Y'); $i++)
                                                <option value="{{ $i }}">{{ $i }}</option>                                    
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2 d-flex align-items-end">
                                        <button class="btn btn-sm btn-primary"><i class="fas fa-search mr-2"></i>Cari</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body p-1">
                    <div class="table-responsive" style="font-size: 10px">
                        <table class="table table-hover table-bordered">
                            <thead class="text-center">
                                <tr>
                                    <th></th>
                                    <th>No Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama Pengirim</th>
                                    <th>Tanggal Pengiriman</th>
                                    <th>Pelanggan</th>
                                    <th>Jumlah Bayar</th>
                                    <th>Tanggal Pembayaran</th>
                                </tr>
                            </thead>
                            <tbody class="border">
                                @forelse ($transaksi as $item)
                                    <tr class="align-middle text-center">
                                        <td style="width: 50px">
                                            {{ $loop->iteration }}
                                        </td>
                                        <td>{{ $item->no_transaksi }}</td>
                                        <td>{{ $item->tanggal_transaksi }}</td>
                                        <td class="text-left">{{ $item->nama_pengirim }}</td>
                                        <td>{{ $item->tanggal_pengiriman }}</td>
                                        <td class="text-left">{{ $item->nama_pelanggan }}</td>
                                        <td class="text-right">{{ $item->jumlah_pembayaran }}</td>
                                        <td>{{ $item->tanggal_pembayaran }}</td>
                                    </tr>                                    
                                @empty
                                    <tr>
                                        <td colspan="8" class="text-center align-middle">Data kosong atau tidak ditemukan.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

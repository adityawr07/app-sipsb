<div>
    <x-slot:title>Data Supplier</x-slot:title>
    @if ($stateForm)
        <div>
            <div class="row d-flex justify-content-center">
                <div class="col-lg-6">
                    <div class="card">
                        <form wire:submit.prevent="submit">
                            <div class="card-header">
                                <h4>Supplier</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group col-12">
                                    <label>Nama Supplier</label>
                                    <input type="text" class="form-control" wire:model.defer="form.nama_supplier">
                                </div>
                                <div class="form-group col-12">
                                    <label>Email</label>
                                    <input type="email" class="form-control" wire:model.defer="form.email">
                                </div>
                                <div class="form-group col-12">
                                    <label>Alamat</label>
                                    <input type="text" class="form-control" wire:model.defer="form.alamat">
                                </div>
                                <div class="form-group col-12">
                                    <label>Kontak/Telepon</label>
                                    <input type="text" class="form-control" wire:model.defer="form.telepon">
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-secondary" wire:click="cancelForm" type="button">Cancel</button>
                                <button class="btn btn-primary">Save Changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>    
    @else
        <button type="button" class="btn btn-primary mb-3" wire:click="addSupplier"><i class="fas fa-plus mr-2"></i> Tambah Supplier</button>
        <div class="card">
            <div class="card-body p-1">
                <div class="table-responsive" style="font-size: 10px">
                    <table class="table table-hover table-bordered">
                        <thead class="text-center">
                            <tr>
                                <th>No</th>
                                <th>Nama Supplier</th>
                                <th>Email</th>
                                <th>Alamat</th>
                                <th>Telepon</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($supplier as $item)
                                <tr>
                                    <td class="text-center align-middle">{{ $loop->iteration }}</td>
                                    <td>{{ $item->nama_supplier }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->alamat }}</td>
                                    <td>{{ $item->telepon }}</td>
                                    <td class="text-center align-middle">
                                        <button class="btn btn-primary btn-action mr-1" wire:click="editSupplier({{ $item->id }})"><i class="fas fa-pencil-alt"></i></button>
                                        <button type="button" class="btn btn-danger btn-action" onclick="deleteSupplier({{ $item->id }})"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>        
    @endif
</div>

@push('scripts')
<script>
    function deleteSupplier(id) {
        Swal.fire({
            text: `Apakah benar anda ingin menghapus data ini?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, Benar',
            cancelButtonText: 'Tidak',
            customClass: {
                confirmButton: 'btn btn-danger',
                cancelButton: 'btn btn-light'
            }
        }).then((result) => {
            if (result.isConfirmed) { 
                Livewire.emit('deleteSupplier', id);
            }
        });        
    }
</script>    
@endpush


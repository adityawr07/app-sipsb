<div>
    <x-slot:title>Data Pengelolaan Sampah</x-slot:title>
    @if ($stateForm)
    <div>
        <div class="row d-flex justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <form wire:submit.prevent="submit">
                        <div class="card-header">
                            <h4>Barang</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group col-12">
                                <label>Kode Barang</label>
                                <input type="text" class="form-control" wire:model.defer="form.kode_barang" disabled>
                            </div>
                            <div class="form-group col-12">
                                <label>Berat Barang (Kg)</label>
                                <input type="text" class="form-control" onkeypress="return angkadesimal(event)" wire:model.defer="form.satuan_barang">
                            </div>
                            <div class="form-group col-12">
                                <label>Kelompok Sampah</label>
                                @php
                                $kelompokSampah = [
                                'Sisa Makanan' => 'Sisa Makanan',
                                'Kayu/Ranting' => 'Kayu/Ranting',
                                'Kertas/Karton' => 'Kertas/Karton',
                                'Plastik' => 'Plastik',
                                'Karet/Kulit' => 'Karet/Kulit',
                                'Kain' => 'Kain',
                                'Kaca' => 'Kaca',
                                'Logam' => 'Logam',
                                'Lainnya' => 'Lainnya',
                                ]
                                @endphp
                                <select wire:model.defer="form.kelompok_barang" class="form-control form-select " id="" data-placeholder="--">
                                    <option value=""></option>
                                    @foreach ($kelompokSampah as $key => $value)
                                    <option value="{{ $key }}" @selected(old('form.kelompok_barang')==$key)>
                                        {{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12">
                                <label>Sumber Sampah</label>
                                @php
                                $sumberSampah = [
                                'Rumah Tangga' => 'Rumah Tangga',
                                'Perkantoran' => 'Perkantoran ',
                                'Pasar Tradisional' => 'Pasar Tradisional',
                                'Fasilitas Publik' => 'Fasilitas Publik',
                                'Lainnya' => 'Lainnya',
                                ]
                                @endphp
                                <select wire:model.defer="form.sumber_barang" class="form-control form-select " id="" data-placeholder="--">
                                    <option value=""></option>
                                    @foreach ($sumberSampah as $key => $value)
                                    <option value="{{ $key }}" @selected(old('form.sumber_barang')==$key)>
                                        {{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12">
                                <label>Kecamatan</label>
                                <input type="text" class="form-control" wire:model.defer="form.kecamatan" disabled>
                            </div>
                            <div class="form-group col-12">
                                <label>Kelurahan</label>
                                <select wire:model.defer="form.kelurahan_id" class="form-control form-select " id="" data-placeholder="--">
                                    <option value=""></option>
                                    @foreach ($kelurahan as $key => $value)
                                    <option value="{{ $key }}" @selected(old('form.kelurahan_id')==$key)>
                                        {{ $value }}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="form-group col-12">
                                <label>RW</label>
                                <input type="text" class="form-control" onkeypress="return hanyaAngka(event)" wire:model.defer="form.rw">
                            </div>
                            <div class="form-group col-12">
                                <label>RT</label>
                                <input type="text" class="form-control" onkeypress="return hanyaAngka(event)" wire:model.defer="form.rt">
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-primary">Simpan</button>
                            <button class="btn btn-danger" wire:click="cancelForm" type="button">Batal</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @else
    <button type="button" class="btn btn-primary mb-3" wire:click="addBarang"><i class="fas fa-plus mr-2"></i> Tambah barang</button>
    <div class="card">
        <div class="card-body p-1">
            <div class="table-responsive" style="font-size: 10px">
                <table class="table table-hover table-bordered">
                    <thead class="text-center">
                        <tr>
                            <th>No</th>
                            <th>Kode Barang</th>
                            <th>Berat Sampah (Kg)</th>
                            <th>Kelompok Sampah</th>
                            <th>Sumber Sampah</th>
                            <th>Kecamatan</th>
                            <th>Kelurahan</th>
                            <th>RW</th>
                            <th>RT</th>
                            <th width=150px></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($barang as $item)
                        <tr>
                            <td class="text-center align-middle">{{ $loop->iteration }}</td>
                            <td>{{ $item->kode_barang }}</td>
                            <td>{{ $item->satuan_barang }}</td>
                            <td>{{ $item->kelompok_barang }}</td>
                            <td>{{ $item->sumber_barang }}</td>
                            <td>{{ $item->kecamatan }}</td>
                            <td>{{ $item->kelurahan->nama }}</td>
                            <td>{{ $item->rw }}</td>
                            <td>{{ $item->rt }}</td>
                            <td class="text-center align-middle">
                                <button class="btn btn-sm btn-primary btn-action mr-1" wire:click="editBarang({{ $item->id }})"><i class="fas fa-pencil-alt"></i></button>
                                <button type="button" class="btn btn-sm btn-danger btn-action" onclick="deleteBarang({{ $item->id }})"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif
</div>

@push('scripts')
<script>
    function deleteBarang(id) {
        Swal.fire({
            text: `Apakah benar anda ingin menghapus data ini?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, Benar',
            cancelButtonText: 'Tidak',
            customClass: {
                confirmButton: 'btn btn-danger',
                cancelButton: 'btn btn-light'
            }
        }).then((result) => {
            if (result.isConfirmed) {
                Livewire.emit('deleteBarang', id);
            }
        });
    }

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
    function angkadesimal(evt) {
        var charCode = (event.which) ? event.which : event.keyCode
        if (charCode == 46) {
            if (txt.value.indexOf(".") < 0)
                return true;
            else
                return false;
        }

        if (txt.value.indexOf(".") > 0) {
            var txtlen = txt.value.length;
            var dotpos = txt.value.indexOf(".");
            //Change the number here to allow more decimal points than 2
            if ((txtlen - dotpos) > 2)
                return false;
        }

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;

    }

</script>
@endpush

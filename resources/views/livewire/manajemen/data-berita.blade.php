<div>
    <x-slot:title>Data Berita</x-slot:title>
    @if ($stateForm)
        <div>
            <div class="row d-flex justify-content-center">
                <div class="col-lg-12">
                    <div class="card">
                        <form wire:submit.prevent="submit">
                            <div class="card-header">
                                <h4>Berita</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group col-12">
                                    <label>Judul Berita</label>
                                    <input type="text" class="form-control" wire:model.defer="form.judul_berita">
                                </div>
                                <div class="form-group col-12">
                                    <label>Isi Berita</label>
                                     <textarea cols="30" rows="10" class="form-control" wire:model.defer="form.isi_berita"></textarea>
                                </div>
                                <div class="form-group col-12">
                                    <label>File Gambar</label>
                                    <input type="file" class="form-control" wire:model.defer="form.foto_berita">
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary">Simpan</button>
                                <button class="btn btn-danger" wire:click="cancelForm" type="button">Batal</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @else
        <button type="button" class="btn btn-primary mb-3" wire:click="addData"><i class="fas fa-plus mr-2"></i> Tambah Data</button>
        <div class="card">
            <div class="card-body p-1">
                <div class="table-responsive" style="font-size: 10px">
                    <table class="table table-hover table-bordered">
                        <thead class="text-center">
                            <tr>
                                <th>No</th>
                                <th>Judul Berita</th>
                                <th>Isi</th>
                                <th width=100px>File</th>
                                <th width=200px></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($berita as $item)
                                <tr>
                                    <td class="text-center align-middle">{{ $loop->iteration }}</td>
                                    <td>{{ $item->judul_berita }}</td>
                                    <td>{{ $item->isi_berita }}</td>
                                    <td class="text-center">

                                        @if (!empty($item->foto_berita))
                                            <a href="{{ asset('storage/'.$item->foto_berita) }}" type="button" class="btn btn-success btn-sm text-center" download><i class="fas fa-download"></i></a>
                                        @else
                                            <h3 class="badge badge-light-danger text-center">Belum Upload Gambar! </h3>
                                        @endif
                                    </td>
                                    {{-- <td>{{ $item->foto_berita }}</td> --}}
                                    <td class="text-center align-middle">
                                        <button class="btn btn-sm btn-primary btn-action mr-1" wire:click="editData({{ $item->id }})"><i class="fas fa-pencil-alt"></i></button>
                                        <button type="button" class="btn btn-sm btn-danger btn-action"  wire:click="deleteBerita({{ $item->id }})" ><i class="fas fa-trash"></i></button>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
</div>

@push('scripts')
<script>
    function deleteData(id) {
        Swal.fire({
            text: `Apakah benar anda ingin menghapus data ini?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, Benar',
            cancelButtonText: 'Tidak',
            customClass: {
                confirmButton: 'btn btn-danger',
                cancelButton: 'btn btn-light'
            }
        }).then((result) => {
            if (result.isConfirmed) {
                Livewire.emit('deleteBErita', id);
            }
        });
    }
</script>
@endpush


<div x-data="TransaksiBarangMasuk" x-cloak @barang-selected.window="getBarang($event.detail.barang)" @transaksi-selected.window="bindAfterSubmit($event.detail.transaksi)">
    <x-slot:title>Transaksi Barang Masuk</x-slot:title>
    <div class="row d-flex justify-content-between gap-lg-1 gap-2 mb-3">
        <div class="col p-0">
            <button class="btn btn-sm" type="button"
                x-bind:class="{ 'btn-primary': !disableAddBtn, 'btn-secondary': disableAddBtn }"
                x-bind:disabled="disableAddBtn" x-on:click="handleAddBtnClick">
                <i class="fas fa-plus mr-2"></i> Tambah
            </button>
            <button class="btn btn-sm" type="button"
                x-bind:class="{ 'btn-warning': !disableEditBtn, 'btn-secondary': disableEditBtn }"
                x-bind:disabled="disableEditBtn" x-on:click="handleUpdateBtnClick">
                <i class="fas fa-edit mr-2"></i> Edit
            </button>
            <button class="btn btn-sm" type="button"
                x-bind:class="{ 'btn-danger': !disableDeleteBtn, 'btn-secondary': disableDeleteBtn }"
                x-bind:disabled="disableDeleteBtn" x-on:click="deleteTransaksi">
                <i class="fas fa-trash mr-2"></i> Hapus
            </button>
            <button class="btn btn-sm" type="button"
                x-bind:class="{ 'btn-success': !disableSaveBtn, 'btn-secondary': disableSaveBtn }"
                x-bind:disabled="disableSaveBtn" x-on:click="submit">
                <i class="fas fa-save mr-2"></i> simpan
            </button>
            <button class="btn btn-sm" type="button"
                x-bind:class="{ 'btn-danger': !disableCancelBtn, 'btn-secondary': disableCancelBtn }"
                x-bind:disabled="disableCancelBtn" x-on:click="clearForm">
                <i class="fas fa-redo mr-2"></i> Batal
            </button>
        </div>
        <div class="col text-right">
            <button class="btn btn-sm btn-primary" x-on:click="showRiwayatModal" type="button">
                <i class="fas fa-search mr-2"></i>Cari Riwayat Transaksi
            </button>
        </div>
    </div>
    <div class="row row-cols-lg-2 g-2">
        <div class="col p-0">
            <div class="card">
                <form>
                    <div class="card-header">
                        <h4>Data Transaksi</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group row mb-1">
                            <label for="inputEmail3" style="font-size: 10px" class="col-sm-3 col-form-label">No Transaksi</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm" x-bind:value="form.no_transaksi" disabled>
                            </div>
                        </div>
                        <div class="form-group row mb-1">
                            <label for="inputPassword3" style="font-size: 10px" class="col-sm-3 col-form-label">Tanggal Transaksi</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control form-control-sm" x-bind:disabled="disableForm" x-model="form.tanggal_transaksi">
                            </div>
                        </div>
                        <div class="form-group row mb-1">
                            <label for="inputEmail3" style="font-size: 10px" class="col-sm-3 col-form-label">Keterangan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm" x-bind:disabled="disableForm" x-model="form.keterangan">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col px-1">
            <div class="card mb-1">
                <form>
                    <div class="card-header">
                        <h4>Data Supplier</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group row mb-1">
                            <label for="inputEmail3" style="font-size: 10px" class="col-sm-3 col-form-label">Nama Supplier</label>
                            <div class="col-sm-9">
                                <select class="form-control form-control-sm p-0" style="
                                    font-size: 10px;
                                    height: 31px;
                                " x-bind:disabled="disableForm" x-model="form.supplier_id">
                                    <option value="">Pilih Supplier</option>
                                    @foreach (getSupplier() as $i => $item)
                                        <option value="{{ $i }}">{{ $item }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row mb-1">
                            <label for="inputPassword3" style="font-size: 10px" class="col-sm-3 col-form-label">Alamat Supplier</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm" x-bind:value="supplier.alamat" disabled>
                            </div>
                        </div>
                        <div class="form-group row mb-1">
                            <label for="inputEmail3" style="font-size: 10px" class="col-sm-3 col-form-label">Telepon</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm" x-bind:value="supplier.telepon" disabled>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row row-cols-lg-2 g-2">
        <div class="col p-0">
            <div class="card mb-1">
                <form>
                    <div class="card-header">
                        <h4>Data Penerima</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group row mb-1">
                            <label for="inputEmail3" style="font-size: 10px" class="col-sm-3 col-form-label">Nama Penerima</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm" x-model="form.nama_penerima" x-bind:disabled="disableForm">
                            </div>
                        </div>
                        <div class="form-group row mb-1">
                            <label for="inputPassword3" style="font-size: 10px" class="col-sm-3 col-form-label">Tanggal Terima</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control form-control-sm" x-bind:disabled="disableForm" x-model="form.tanggal_penerimaan">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col px-1">
            <div class="card mb-1">
                <form>
                    <div class="card-header">
                        <h4>Data Pembayaran</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group row mb-1">
                            <label for="inputEmail3" style="font-size: 10px" class="col-sm-3 col-form-label">Jumlah Bayar</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control form-control-sm text-right" x-model="form.jumlah_pembayaran" x-bind:disabled="disableForm">
                            </div>
                        </div>
                        <div class="form-group row mb-1">
                            <label for="inputPassword3" style="font-size: 10px" class="col-sm-3 col-form-label">Tanggal Bayar</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control form-control-sm" x-bind:disabled="disableForm" x-model="form.tanggal_pembayaran">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col p-0">
            <div class="card mb-1">
                <div class="card-header">
                    <h4>Rincian Barang Masuk</h4>
                    <div class="card-header-action">
                        <button class="btn" x-bind:disabled="disableForm"
                            x-bind:class="{ 'btn-primary': !disableForm, 'btn-secondary': disableForm }"
                            x-on:click="addRow" type="button"><i class="fas fa-plus mr-2"></i> Tambah Barang
                        </button>
                        <button class="btn" x-bind:disabled="disableForm" 
                            x-bind:class="{ 'btn-danger': !disableForm, 'btn-secondary': disableForm }"
                            x-on:click="form.items.splice(selectedItemIndex, 1)" type="button"><i class="fas fa-times mr-2"></i> Hapus Barang
                        </button>
                    </div>
                </div>
                <div class="card-body" x-bind:class="{ 'p-0': form.items.length !== 0, 'p-3': form.items.length === 0 }">
                    <template x-if="form.items.length === 0">
                        <div class="alert alert-primary alert-has-icon">
                            <div class="alert-icon mr-3"><i class="fas fa-exclamation-triangle"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Informasi</div>
                                Silahkan tambah barang masuk.
                            </div>
                        </div>
                    </template>
                    <template x-if="form.items.length !== 0">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead class="text-center">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Barang</th>
                                        <th>Kode Barang</th>
                                        <th>Jumlah</th>
                                        <th>Satuan</th>
                                    </tr>
                                </thead>
                                <tbody class="border">
                                    <template x-for="(item, index) in form.items">
                                        <tr @click="selectedItemIndex = index"
                                            :class="{ 'bg-light': selectedItemIndex === index }">
                                            <td class="text-center align-middle">
                                                <span x-text="index + 1"></span>
                                            </td>
                                            <td class="text-center align-middle">
                                                <input type="text" class="form-control form-control-sm min-h-auto border-0 p-0" x-model="item.nama_barang"
                                                    placeholder="Ketik kemudian enter" x-on:keyup.enter="handleNamaBarangEnter(item.nama_barang)">
                                            </td>
                                            <td class="text-center align-middle">
                                                <span x-text="item.kode_barang"></span>
                                            </td>
                                            <td class="text-center align-middle" style="width: 150px">
                                                <input type="number" x-model="item.qty" class="form-control form-control-sm min-h-auto border-0 p-0 text-right">
                                            </td>
                                            <td class="text-center align-middle">
                                                <span x-text="item.satuan_barang"></span>
                                            </td>
                                        </tr>
                                    </template>
                                </tbody>
                            </table>
                        </div>
                    </template>
                </div>
            </div>
        </div>
    </div>
    <x-transaksi-barang-masuk-modal />
    <x-barang-modal />
</div>


@push('scripts')
<script>
    var tanggal = {{ Js::from(date('Y-m-d', strtotime(now()))) }};
    function TransaksiBarangMasuk() {
        return {
            selectedItemIndex: '',
            disableForm: true,
            disableAddBtn: false,
            disableSaveBtn: true,
            disableEditBtn: true,
            disableCancelBtn: true,
            disableDeleteBtn: true,
            form: {
                tanggal_transaksi: tanggal,
                keterangan: '',
                supplier_id: '',
                nama_penerima: '',
                tanggal_penerimaan: '',
                jumlah_pembayaran: '',
                tanggal_pembayaran: '',
                items: [],
            },
            supplier: new Object(),
            barangModal: '',
            transaksiBarangMasukModal: '',
            init() {
                this.transaksiBarangMasukModal = new bootstrap.Modal(document.getElementById('transaksiBarangMasukModal'));
                this.barangModal = new bootstrap.Modal(document.getElementById('barangModal'));
                this.$watch('form.supplier_id', val => {
                    this.getSupplier(val);
                });
            },
            async getSupplier(id) {
                const { data } = await axios.get(`/supplier/${id}`);
                this.supplier = data;
            },
            async getBarang(id) {
                const { data } = await axios.get(`/barang/${id}`);
                this.form.items[this.selectedItemIndex].barang_id = data.id;
                this.form.items[this.selectedItemIndex].nama_barang = data.nama_barang;
                this.form.items[this.selectedItemIndex].kode_barang = data.kode_barang;
                this.form.items[this.selectedItemIndex].satuan_barang = data.satuan_barang;
            },
            handleAddBtnClick() {
                this.disableForm = false;
                this.disableSaveBtn = false;
                this.disableCancelBtn = false;
                this.disableAddBtn = true;
            },
            handleEditBtnClick() {
                this.disableForm = true;
                this.disableAddBtn = true;
                this.disableSaveBtn = true;
                this.disableEditBtn = false;
                this.disableDeleteBtn = false;
                this.disableCancelBtn = false;
            },
            handleUpdateBtnClick() {
                this.disableForm = false;
                this.disableSaveBtn = false;
                this.disableCancelBtn = false;
                this.disableDeleteBtn = true;
                this.disableEditBtn = true;
                this.is_edit = true;
            },
            handleCancelBtnClick() {
                this.disableForm = true;
                this.disableSaveBtn = true;
                this.disableCancelBtn = true;
                this.disableDeleteBtn = true;
                this.disableEditBtn = true;
                this.disableAddBtn = false;
            },
            handleNamaBarangEnter(barang) {
                this.$dispatch('toggle-barang-modal', { show: true, barang: barang });
                $('#barangModal').appendTo("body").modal('show');
            },
            showRiwayatModal() {
                this.$dispatch('toggle-transaksi-barang-masuk-modal', { show: true });
                $('#transaksiBarangMasukModal').appendTo("body").modal('show');
            },
            addRow() {
                this.form.items.push({
                    'barang_id': '',
                    'qty': 0,
                });
            },
            clearForm() {
                this.form.id = '';
                this.form.no_transaksi = '';
                this.form.tanggal_transaksi = tanggal;
                this.form.keterangan = '';
                this.form.supplier_id = '';
                this.form.nama_penerima = '';
                this.form.tanggal_penerimaan = '';
                this.form.jumlah_pembayaran = '';
                this.form.tanggal_pembayaran = '';
                this.form.items = [];
                this.supplier = {};
                this.handleCancelBtnClick();
            },
            async submit() {
                try {
                    const { data } = await axios.post('/transaksi/barang-masuk', this.form);
                    Livewire.emit('alert-success', 'Berhasil Disimpan.');
                    this.bindAfterSubmit(data);
                } catch (error) {
                    if (error.response.status && error.response.status == 422) {
                        Livewire.emit('alert-error', error.response.data);
                        return;
                    }
                }
                
            },
            bindAfterSubmit(data) {
                this.handleEditBtnClick();
                this.form.id = data.id;
                this.form.no_transaksi = data.no_transaksi;
                this.form.tanggal_transaksi = data.tanggal_transaksi;
                this.form.keterangan = data.keterangan;
                this.form.supplier_id = data.supplier_id;
                this.form.nama_penerima = data.nama_penerima;
                this.form.tanggal_penerimaan = data.tanggal_penerimaan;
                this.form.jumlah_pembayaran = data.jumlah_pembayaran;
                this.form.tanggal_pembayaran = data.tanggal_pembayaran;
                this.form.items = [];
                data.items.forEach(element => {
                    this.form.items.push({
                        'id': element.id,
                        'barang_id': element.barang_id,
                        'nama_barang': element.barang.nama_barang,
                        'kode_barang': element.barang.kode_barang,
                        'satuan_barang': element.barang.satuan_barang,
                        'qty': element.qty,
                    })
                });
            },
            deleteTransaksi() {
                if (this.form.id) {
                    Swal.fire({
                        title: 'Yakin akan dihapus?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Ya, hapus!',
                        cancelButtonText: 'Tidak',
                        buttonsStyling: false,
                        customClass: {
                            confirmButton: 'btn btn-danger mr-3',
                            cancelButton: 'btn btn-light'
                        }
                    }).then(async (result) => {
                        if (result.isConfirmed) {
                            this.delete(this.form.id);
                        }
                    });
                }
            },
            async delete(id) {
                await axios.delete('/api/delete-transaksi-barang-masuk', { data: { id: id } });
                this.clearForm();
                this.handleCancelBtnClick();
                Livewire.emit('alert-success', 'Berhasil dihapus!');
            }
        }
    }
</script>
@endpush
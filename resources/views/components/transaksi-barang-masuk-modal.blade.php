<div class="modal fade" tabindex="-1" role="dialog" id="transaksiBarangMasukModal" aria-labelledby="transaksiBarangMasuk" aria-hidden="true"
    x-on:toggle-transaksi-barang-masuk-modal.window="getTransaksi($event.detail)" x-data="TransaksiBarangMasukModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Riwayat Transaksi Barang Masuk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-0">
                <div class="table-responsive">
                    <table class="table table-sm table-hover table-bordered">
                        <thead class="text-center bg-whitesmoke">
                            <tr>
                                <th></th>
                                <th>No Transaksi</th>
                                <th>Tanggal Transaksi</th>
                                <th>Nama Penerima</th>
                                <th>Jumlah Bayar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template x-if="transaksi.length === 0">
                                <tr>
                                    <td colspan="5" class="text-center align-middle">Data kosong atau tidak ditemukan.</td>
                                </tr>
                            </template>
                            <template x-for="(item, index) in transaksi">
                                <tr>
                                    <td class="text-center align-middle">
                                        <button class="btn btn-sm btn-primary" type="button" x-on:click="rowClick(item)"><i class="fas fa-check mr-2"></i> Pilih</button>
                                    </td>
                                    <td class="text-center align-middle">
                                        <span x-text="item.no_transaksi"></span>
                                    </td>
                                    <td class="text-center align-middle">
                                        <span x-text="item.tanggal_transaksi"></span>
                                    </td>
                                    <td class="text-center align-middle">
                                        <span x-text="item.nama_penerima"></span>
                                    </td>
                                    <td class="text-center align-middle">
                                        <span x-text="item.jumlah_pembayaran"></span>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    function TransaksiBarangMasukModal() {
        return {
            transaksi: [],
            async getTransaksi() {
                const { data } = await axios.get('/api/riwayat-transaksi-barang-masuk');
                this.transaksi = data;
            },
            rowClick(item) {
                $('#transaksiBarangMasukModal').modal('hide');
                this.$dispatch('transaksi-selected', { transaksi: item });
            }
        }
    }
</script>
@endpush
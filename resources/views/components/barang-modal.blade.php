<div class="modal fade" tabindex="-1" role="dialog" id="barangModal" aria-labelledby="barangModal" aria-hidden="true"
    x-on:toggle-barang-modal.window="handleToggleModal($event.detail)" x-data="BarangModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pilih Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-0">
                <div class="table-responsive">
                    <table class="table table-sm table-hover table-bordered">
                        <thead class="text-center bg-whitesmoke">
                            <tr>
                                <th></th>
                                <th>Nama Barang</th>
                                <th>Kode Barang</th>
                                <th>Satuan Barang</th>
                                <th>Kelompok Barang</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template x-if="barang.length === 0">
                                <tr>
                                    <td colspan="5" class="text-center align-middle">Barang kosong atau tidak ditemukan.</td>
                                </tr>
                            </template>
                            <template x-for="(item, index) in barang">
                                <tr>
                                    <td class="text-center align-middle">
                                        <button class="btn btn-sm btn-primary" type="button" x-on:click="rowClick(item)"><i class="fas fa-check mr-2"></i> Pilih</button>
                                    </td>
                                    <td class="align-middle">
                                        <span x-text="item.nama_barang"></span>
                                    </td>
                                    <td class="text-center align-middle">
                                        <span x-text="item.kode_barang"></span>
                                    </td>
                                    <td class="text-center align-middle">
                                        <span x-text="item.satuan_barang"></span>
                                    </td>
                                    <td class="text-center align-middle">
                                        <span x-text="item.kelompok_barang"></span>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    function BarangModal() {
        return {
            show: false,
            keyword: '',
            barang: [],
            init() {
                this.getBarang();
                this.$watch('keyword', val => this.getBarang());
            },
            handleToggleModal(event) {
                this.keyword = event.barang;
            },
            async getBarang() {
                const { data } = await axios.get('/list-barang', {
                    params: {
                        keyword: this.keyword,
                    }
                });
                this.barang = data;
            },
            rowClick(item) {
                this.keyword = '';
                $('#barangModal').modal('hide');
                this.$dispatch('barang-selected', { barang: item.id });
            }
        }
    }
</script>
@endpush
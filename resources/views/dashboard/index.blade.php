<?php
use Carbon\Carbon;
?>
<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>SIPSB</title>

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/sipsb.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link id="googleFonts" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400&display=swap" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="vendor/animate/animate.compat.css">
		<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
        <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">
        <link rel="stylesheet" href="dist/apexcahrts.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">
		<link rel="stylesheet" href="css/theme-blog.css">
		<link rel="stylesheet" href="css/theme-shop.css">

		<!-- Revolution Slider CSS -->
		<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

		<!-- Demo CSS -->
		<link rel="stylesheet" href="css/demos/demo-digital-agency.css">

		<!-- Skin CSS -->
		<link id="skinCSS" rel="stylesheet" href="css/skins/skin-digital-agency.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">

		<!-- Head Libs -->
        <script src="vendor/modernizr/modernizr.min.js"></script>
        <script src="dist/apexcharts.min.js"></script>

	</head>
	<body>

		<div class="body">
			<header id="header" class="header-transparent header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyChangeLogo': false, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
				<div class="header-body border-top-0 bg-dark box-shadow-none">
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo text-center">
										<a href="index.html">
											<img alt="SIPSB" width="50" height="50" src="img/sipsb.png">

										</a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row">
									<div class="header-nav header-nav-links header-nav-line header-nav-bottom-line header-nav-bottom-line-active-text-light header-nav-dropdowns-dark header-nav-light-text">
										<div class="header-nav-main header-nav-main-text-capitalize header-nav-main-mobile-dark header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
											<nav class="collapse">
												<ul class="nav nav-pills" id="mainNav">
													<li>
														<a class="nav-link active" href="{{ url('/') }}">
															Beranda
														</a>
													</li>
													<li>
														<a class="nav-link" href="{{ url('/visi-misi') }}">
															Visi Misi
														</a>
													</li>
													<li>
														<a class="nav-link" href="{{ url('/berita') }}">
															Berita
														</a>
													</li>
													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle">
															Data Pengelolaan Sampah
														</a>
														<ul class="dropdown-menu">
															<li><a class="dropdown-item" href="{{ url('/timbunan-sampah') }}">Timbunan Sampah</a></li>
															<li><a class="dropdown-item" href="{{ url('/komposisi-sampah') }}">Komposisi Sampah</a></li>
															<li><a class="dropdown-item" href="{{ url('/sumber-sampah') }}">Sumber Sampah</a></li>
														</ul>
													</li>
													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle">
															Fasilitas Pengelolaan Sampah
														</a>
														<ul class="dropdown-menu">
															<li><a class="dropdown-item" href="{{ url('/bank-sampah') }}">Bank Sampah</a></li>
															<li><a class="dropdown-item" href="{{ url('/rumah-kompos') }}">Rumah Kompos</a></li>
															<li><a class="dropdown-item" href="{{ url('/tps3r') }}">TPS3R</a></li>
														</ul>
													</li>
													<li>
														<a class="nav-link" href="{{ url('/kontak') }}">
															Hubungi Kami
														</a>
													</li>
												</ul>
											</nav>
										</div>
									</div>

									<button class="btn header-btn-collapse-nav ms-3 ms-sm-4" data-bs-toggle="collapse" data-bs-target=".header-nav-main nav">
										<i class="fas fa-bars"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div role="main" class="main">

				<div class="owl-carousel owl-carousel-light owl-carousel-light-init-fadeIn owl-theme manual dots-inside dots-horizontal-center custom-dots-style-1 dots-light show-dots-hover show-dots-xs show-dots-sm show-dots-md nav-style-1 nav-inside nav-inside-plus nav-light nav-lg nav-font-size-lg show-nav-hover bg-color-quaternary custom-slider-container mb-0" data-plugin-options="{'autoplay': false, 'autoplayTimeout': 7000}" data-dynamic-height="['780px','780px','780px','780px','510px']" style="height: 780px;">
					<div class="owl-stage-outer">
						<div class="owl-stage">

							<!-- Carousel Slide 1 -->
							<div class="owl-item position-relative overflow-hidden">
								<img src="img/bgsiresma.jpeg" class="img-fluid custom-bg-slider-right" alt="" />
								<div class="custom-svg-style-1 svg-fill-color-primary position-absolute top-0 left-50pct transform3dx-n50 h-100 z-index-0">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 799" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" data-plugin-float-element-svg="true">
										<path id="svg_1" d="m803,799c-0.65,-17.51 -0.69,-41.58 2,-70c7.66,-80.82 31.97,-139.52 47,-175c30.86,-72.87 67.88,-12.83 89,-152c17.86,-22.98 84.33,-105.42 201,-174c34.75,-20.42 121.02,-67.28 243,-90c48.07,-8.95 141.2,-21.52 256,-4c123.54,18.85 208.91,62.88 228,73c21.42,11.35 38.74,22.03 51,30c0,-79 0,-158 0,-237c-640,0 -1280,0 -1920,0c0,266.33 0,532.67 0,799c267.67,0 535.33,0 803,0z" fill="#48C97F"/>
										<circle id="svg_2" r="7.5" cy="539.5" cx="209.5" fill="#FFF" opacity="0.2" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.3, 'transition': true, 'transitionDuration': 1000, 'isInsideSVG': true}"/>
										<circle id="svg_3" r="12" cy="211" cx="268" fill="#FFF" opacity="0.2" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.4, 'transition': true, 'transitionDuration': 2000, 'isInsideSVG': true}"/>
										<circle id="svg_4" r="17" cy="144" cx="1864" fill="#FFF" opacity="0.2" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.6, 'transition': true, 'transitionDuration': 1000, 'isInsideSVG': true}"/>
										<path id="svg_5" d="m982,356c24.33,-22.33 48.67,-44.67 73,-67c-1.91,-2.73 -5.1,-6.6 -10,-10c-7.11,-4.93 -13.93,-6.15 -19,-7c-5.42,-0.91 -14.32,-2.3 -25,1c-4.46,1.38 -16.54,5.83 -25,18c-5.85,8.41 -7.29,16.65 -8,21c-0.51,3.13 -1.54,9.79 0,18c2.48,13.19 10.04,22.01 14,26z" fill="#FFF" opacity="0.2" style="fill: #FFF !important;"/>
										<circle id="svg_6" r="28.5" cy="326.74992" cx="327.75073" stroke-miterlimit="10" stroke-width="3" stroke="6CD499" fill="none"/>
										<circle opacity="0.2" stroke="#ffffff" id="svg_8" r="21.5" cy="340.25" cx="90.74976" fill="none" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.3, 'transition': true, 'transitionDuration': 1000, 'isInsideSVG': true}"/>
										<circle stroke="#ffffff" opacity="0.2" id="svg_9" r="14.625" cy="689.625" cx="128.87476" fill="none" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.5, 'transition': true, 'transitionDuration': 2000, 'isInsideSVG': true}"/>
									</svg>
								</div>
								<div class="container position-relative h-100 z-index-1 mt-4">
									<div class="row align-items-center h-100">
										<div class="col mb-5-5 mb-sm-0">
											<div class="appear-animation" data-appear-animation="fadeInDownShorterPlus" data-appear-animation-delay="800">
												<h1 class="text-color-light font-weight-bold positive-ls-3 custom-big-text-2 line-height-1 mb-0">SIPSB</h1>
											</div>
											<p class="text-color-dark font-weight-bold text-7 text-sm-9 ms-2 mb-3 appear-animation" data-appear-animation="fadeInDownShorterPlus" data-appear-animation-delay="1000">Sistem Informasi Pengelolaan</p>
											<p class="text-color-dark font-weight-bold text-7 text-sm-9 ms-2 mb-3 appear-animation" data-appear-animation="fadeInDownShorterPlus" data-appear-animation-delay="1000">Sampah Kecamatan</p>
											<p class="text-color-dark font-weight-bold text-7 text-sm-9 ms-2 mb-3 appear-animation" data-appear-animation="fadeInDownShorterPlus" data-appear-animation-delay="1000">Semarang Barat</p>
										</div>
									</div>
								</div>
							</div>

							<!-- Carousel Slide 2 -->
							<div class="owl-item position-relative overflow-hidden">
								<img src="img/bgsiresma.jpeg" class="img-fluid custom-bg-slider-left" alt="" />
								<div class="custom-svg-style-1 custom-svg-style-1-variation custom-svg-reverse svg-fill-color-dark position-absolute top-0 left-50pct transform3dx-n50 h-100 z-index-0">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 799" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" data-plugin-float-element-svg="true">
										<path id="svg_1" d="m803,799c-0.65,-17.51 -0.69,-41.58 2,-70c7.66,-80.82 31.97,-139.52 47,-175c30.86,-72.87 67.88,-124.83 89,-152c17.86,-22.98 84.33,-105.42 201,-174c34.75,-20.42 121.02,-67.28 243,-90c48.07,-8.95 141.2,-21.52 256,-4c123.54,18.85 208.91,62.88 228,73c21.42,11.35 38.74,22.03 51,30c0,-79 0,-158 0,-237c-640,0 -1280,0 -1920,0c0,266.33 0,532.67 0,799c267.67,0 535.33,0 803,0z" fill="#48C97F"/>
										<circle id="svg_2" r="7.5" cy="539.5" cx="209.5" fill="#FFF" opacity="0.2" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.3, 'transition': true, 'transitionDuration': 1000, 'isInsideSVG': true}"/>
										<circle id="svg_3" r="12" cy="211" cx="268" fill="#FFF" opacity="0.2" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.4, 'transition': true, 'transitionDuration': 2000, 'isInsideSVG': true}"/>
										<circle id="svg_4" r="17" cy="144" cx="1864" fill="#FFF" opacity="0.2" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.2, 'transition': true, 'transitionDuration': 1000, 'isInsideSVG': true}"/>
										<path id="svg_5" d="m982,356c24.33,-22.33 48.67,-44.67 73,-67c-1.91,-2.73 -5.1,-6.6 -10,-10c-7.11,-4.93 -13.93,-6.15 -19,-7c-5.42,-0.91 -14.32,-2.3 -25,1c-4.46,1.38 -16.54,5.83 -25,18c-5.85,8.41 -7.29,16.65 -8,21c-0.51,3.13 -1.54,9.79 0,18c2.48,13.19 10.04,22.01 14,26z" fill="#FFF" opacity="0.2" style="fill: #FFF !important;"/>
										<circle id="svg_6" r="28.5" cy="326.74992" cx="327.75073" stroke-miterlimit="10" stroke-width="3" stroke="6CD499" fill="none" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.3, 'transition': true, 'transitionDuration': 1000, 'isInsideSVG': true}"/>
										<circle opacity="0.2" stroke="#ffffff" id="svg_8" r="21.5" cy="340.25" cx="90.74976" fill="none" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.4, 'transition': true, 'transitionDuration': 2000, 'isInsideSVG': true}"/>
										<circle stroke="#ffffff" opacity="0.2" id="svg_9" r="14.625" cy="689.625" cx="128.87476" fill="none" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.5, 'transition': true, 'transitionDuration': 1000, 'isInsideSVG': true}"/>
									</svg>
								</div>
								<div class="container position-relative h-100 z-index-1 mt-4">
									<div class="row align-items-center h-100">
										<div class="col mb-5-5 mb-sm-0 text-end">
											<div class="appear-animation" data-appear-animation="fadeInDownShorterPlus" data-appear-animation-delay="800">
												<h1 class="text-color-light font-weight-bold positive-ls-3 text-12 text-sm-17 line-height-1 mb-3">SIPSB</h1>
											</div>
											<p class="text-color-primary font-weight-bold text-7 text-sm-9 ms-2 mb-3 appear-animation" data-appear-animation="fadeInDownShorterPlus" data-appear-animation-delay="1000">Sistem Informasi Pengelolaan</p>
											<p class="text-color-primary font-weight-bold text-7 text-sm-9 ms-2 mb-3 appear-animation" data-appear-animation="fadeInDownShorterPlus" data-appear-animation-delay="1000">Sampah Kecamatan</p>
											<p class="text-color-primary font-weight-bold text-7 text-sm-9 ms-2 mb-3 appear-animation" data-appear-animation="fadeInDownShorterPlus" data-appear-animation-delay="1000">Semarang Barat</p>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="owl-nav">
						<button type="button" role="presentation" class="owl-prev"></button>
						<button type="button" role="presentation" class="owl-next"></button>
					</div>
					<div class="owl-dots mb-5">
						<button role="button" class="owl-dot active"><span></span></button>
						<button role="button" class="owl-dot"><span></span></button>
					</div>
				</div>

				<div class="container text-center py-5 my-4">
					<div class="col text-center">
						<h2 class="text-color-dark font-weight-bold text-9 mb-4 appear-animation" data-appear-animation="fadeInUpShorterPlus" data-appear-animation-delay="600">CAPAIAN KINERJA PENGELOLAAN SAMPAH</h2>
					</div>
					<div class="col text-center">
						<h3 class="text-color-dark  text-4 mb-4 appear-animation" data-appear-animation="fadeInUpShorterPlus" data-appear-animation-delay="600">Capaian Kinerja Pengelolaan Sampah adalah Capaian Pengurangan dan Penanganan Sampah Rumah Tangga dan Sampah Sejenis Sampah Rumah Tangga.</h3>
					</div>
					<div class="col text-center">
						<h3 class="text-color-dark  text-4 mb-4 appear-animation" data-appear-animation="fadeInUpShorterPlus" data-appear-animation-delay="600">Data capaian dibawah ini adalah hasil dari penginputan data yang dilakukan oleh 200 Kabupaten/kota se-Indonesia pada tahun 2023</h3>
					</div>
				<div class="row counters text-dark mb-4 mt-4 justify-content-center">
					<div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
						<div class="counter">
							<strong  data-to='{{(int)$total}}' data-append=" Kg" class="text-5 mb-1"></strong>
							<label class="font-weight-medium text-4 opacity-5">Timbulan Sampah (ton / Tahun)</label>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
						<div class="counter">
							<strong data-to="0" data-append=" Kg" class="text-5 mb-1">0</strong>
							<label class="font-weight-medium text-4 opacity-5">Pengurangan Sampah (ton / Tahun)</label>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3 mb-4 mb-sm-0">
						<div class="counter">
							<strong data-to="0" data-append=" Kg" class="text-5 mb-1">0</strong>
							<label class="font-weight-medium text-4 opacity-5">Penanganan Sampah (ton / Tahun)</label>
						</div>
					</div>
				</div>
					<div class="row counters text-dark justify-content-center">
						<div class="col-sm-6 col-lg-3">
							<div class="counter">
								<strong data-to="{{(int)$total}}" data-append=" Kg" class="text-5 mb-1">0</strong>
								<label class="font-weight-medium text-4 opacity-5">Sampah Terkelola (ton / Tahun)</label>
							</div>
						</div>
						<div class="col-sm-6 col-lg-3">
							<div class="counter">
								<strong data-to="{{(int)$total}}" data-append=" Kg" class="text-5 mb-1">0</strong>
								<label class="font-weight-medium text-4 opacity-5">Sampah Tidak Terkelola (ton / Tahun)</label>
							</div>
						</div>
					</div>
				</div>

				<div class="container py-5 my-4">
					<div class="row">
						<div class="col">
							<div class="col text-center">
								<h2 class="text-color-dark font-weight-bold text-9 mb-4 appear-animation" data-appear-animation="fadeInUpShorterPlus" data-appear-animation-delay="600">GRAFIK KOMPOSISI SAMPAH</h2>
							</div>

							<section id="grafik" class="grafik">
								<div class="container">

									<div class="col text-center">
										<h3 class="text-color-dark  text-4 mb-4 appear-animation" data-appear-animation="fadeInUpShorterPlus" data-appear-animation-delay="600">Grafik Komposisi Sampah terbagi 2 yaitu Grafik Komposisi Sampah berdasarkan Jenis Sampah dan Grafik Komposisi Sampah berdasarkan Sumber Sampah. Grafik Komposisi Sampah dibawah ini adalah Tahun {{$date}}.</h3>
									</div>

									<div class="row">
										<div class="col-lg-6 col-md-6" style="padding: 10px;">
                                            <div class="box box-solid" style="box-shadow: 10px 10px 10px rgba(0,0,0,0.5);">
                                            <strong class="d-block text-color-dark">Grafik Komposisi Sampah</strong>
                                            <div id="chart"></div>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 mt-6 mt-md-0" style="padding: 10px; ">
                                            <div class="box box-solid" style="box-shadow: 10px 10px 10px rgba(0,0,0,0.5);">
                                            <strong class="d-block text-color-dark">Grafik Sumber Sampah</strong>
                                            <div id="chart2"></div>
											</div>
										</div>
									</div>

								</div>
							</section>

						</div>
					</div>
				</div>

				<section class="section section-height-3 bg-gradient border-0 m-0">
					<div class="container py-1">
						<div class="col">
							<h2 class="text-color-light font-weight-bold text-9 mb-5-5 appear-animation" data-appear-animation="fadeInUpShorterPlus" data-appear-animation-delay="200">Berita</h2>
							<div class="custom-half-carousel-style-1  carousel-half-full-width-right">
								<div class="owl-carousel owl-theme carousel-half-full-width-right dots-align-left dots-light custom-dots-style-1 mb-0" data-plugin-options="{'responsive': {'0': {'items': 1}, '768': {'items': 3}, '992': {'items': 3}, '1200': {'items': 3}}, 'loop': false, 'nav': false, 'dots': true, 'margin': 15}">
									@foreach ($data as $item)
                                    <div class="appear-animation" data-appear-animation="fadeInLeftShorterPlus" data-appear-animation-delay="400">
										<div class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-centered-icons rounded-0 mb-3">
											<div class="thumb-info-wrapper rounded-0">
												<img width="200px" height="200px" src="{{ asset('storage/'.$item->foto_berita) }}" class=" rounded-0" alt="" />
												<div class="thumb-info-action">

												</div>
											</div>
										</div>
										<h2 class="text-color-light font-weight-semibold text-3 opacity-7 line-height-1 mb-1">{{ $item->created_at->translatedFormat('j F Y'); }}</h2>
										<h3 class="text-transform-none font-weight-bold text-5-5 mb-0 pb-2 ">
											<span class="text-decoration-none text-color-light opacity-hover-8 ">{{ $item->judul_berita }}
										</h3>
									</div>
                                    @endforeach

								</div>
							</div>
						</div>
					</div>
				</section>



			</div>
			<footer id="footer" class="mt-0">
				<div class="container py-5 my-4">
					<div class="row gy-4">
						<div class="col-lg-2 align-self-center mb-4 mb-lg-0 text-center">
							<a href="index.html" class="text-decoration-none">
								<img src="img/sipsb.png" width="125" height="35" class="img-fluid" alt="Footer Logo" />
							</a>
						</div>
						<div class="col-lg-3">
							<h4 class="font-weight-bold text-color-dark">About Us</h4>
							<p class="text-3-5">
							<a class="dropdown-item" href="{{ url('/admin') }}"><strong class="d-block text-color-dark">Sistem Informasi Pengelolaan Sampah <br> Kecamatan Semarang Barat</strong></a>

								<!-- Gedung Manggala Wanabakti, <br>
								Blok IV Lantai 5.Jalan Gatot Subroto <br>
								Jakarta 10270, Indonesia<br><br> -->
								Phone: <a href="tel:+62 21 57902763" class="text-decoration-none">+62 21 57902763</a><br>
								<!-- Email: <a href="mailto:sipsn@menlhk.go.id" class="text-decoration-none">sipsn@menlhk.go.id</a> -->
							</p>
						</div>
						<div class="col-lg-3">

						</div>
						<div class="col-lg-3">
							<img src="img/logo_g3jks.png" width="125" height="35" class="img-fluid" alt="Logo G3JKS" />
							<img src="img/logo_bs.png" width="125" height="35" class="img-fluid" alt="Logo BS" />
							<img src="img/logo_ksp.png" width="125" height="35" class="img-fluid" alt="Logo KSP" />
							<img src="img/logo_gnpsdr.png" width="125" height="35" class="img-fluid" alt="Logo GNPSDR" />
						</div>
					</div>
				</div>
				<div class="footer-copyright bg-color-dark-scale-2">
					<div class="container py-2">
						<div class="row py-4">
							<div class="col-md-4 d-flex align-items-center justify-content-center justify-content-md-start mb-2 mb-lg-0">
								<p class="text-color-grey text-3-5">Sistem Informasi Pengelolaan Sampah Kecamatan Semarang Barat © 2023. All Rights Reserved.</p>
							</div>
							<div class="col-md-8 d-flex align-items-center justify-content-center justify-content-md-end mb-4 mb-lg-0">
								<ul class="footer-social-icons social-icons social-icons-clean social-icons-icon-light social-icons-big">
									<li class="social-icons-instagram"><a href="http://www.instagram.com/" target="_blank" title="Intagram"><i class="fab fa-instagram text-4"></i></a></li>
									<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter text-4"></i></a></li>
									<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f text-4"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="vendor/plugins/js/plugins.min.js"></script>

		<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="js/views/view.contact.js"></script>

		<!-- Theme Custom -->
		<script src="js/custom.js"></script>

		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>

	</body>



    <script src="dist/apexcharts.min.js"></script>


<script>
    var chart = document.querySelector("#chart")
    var options = {
          series: [44, 55, 13, 43, 22 , 10 ,12 ,12 ,10],
          chart: {
          width: 485,
          type: 'pie',
        },
        labels: ['Sisa Makanan', 'Kayu/Ranting', 'Kertas/Karton', 'Plastik', 'Karet / Kulit' ,'Kain' ,'Kaca' , 'Logam', 'Lainnya'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 400
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };
        var options2 = {
          series: [44, 55, 13, 43, 22],
          chart: {
          width: 500,
          type: 'pie',
        },
        labels: ['Rumah Tangga', 'Perkantoran', 'Pasar Tradisional', 'Fasilitas Publik', 'Lainnya'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 400
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };


        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
        var chart = new ApexCharts(document.querySelector("#chart2"), options2);
        chart.render();
</script>

</html>
@push('scripts')
<script src="/js/highcharts.js"></script>
<script src="/js/modules/stock.js"></script>
<script src="/js/modules/map.js"></script>
<script src="/js/modules/gantt.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
@endpush


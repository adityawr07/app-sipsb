<!DOCTYPE html>
<?php

use Carbon\Carbon;
?>
<html>

<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Timbunan Sampah</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/sipsb.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
    <link id="googleFonts" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400&display=swap" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="vendor/animate/animate.compat.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">
    <link rel="stylesheet" href="dist/apexcharts.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">

    <!-- Revolution Slider CSS -->
    <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

    <!-- Demo CSS -->
    <link rel="stylesheet" href="css/demos/demo-digital-agency.css">

    <!-- Skin CSS -->
    <link id="skinCSS" rel="stylesheet" href="css/skins/skin-digital-agency.css">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.min.js"></script>
    <script src="dist/apexcharts.min.js"></script>


    <!-- Head Libs -->
    @livewireStyles
    <livewire:styles />

</head>

<body>

    <div class="body">
        <header id="header" class="header-transparent header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyChangeLogo': false, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
            <div class="header-body border-top-0 bg-dark box-shadow-none">
                <div class="header-container container">
                    <div class="header-row">
                        <div class="header-column">
                            <div class="header-row">
                                <div class="header-logo text-center">
                                    <a href="index.html">
                                        <img alt="SIPSB" width="50" height="50" src="img/sipsb.png">
                                        <!-- <strong class="d-block text-color-light">SIPSB</strong> -->
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="header-column justify-content-end">
                            <div class="header-row">
                                <div class="header-nav header-nav-links header-nav-line header-nav-bottom-line header-nav-bottom-line-active-text-light header-nav-dropdowns-dark header-nav-light-text">
                                    <div class="header-nav-main header-nav-main-text-capitalize header-nav-main-mobile-dark header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                        <nav class="collapse">
                                            <ul class="nav nav-pills" id="mainNav">
                                                <li>
                                                    <a class="nav-link " href="{{ url('/') }}">
                                                        Beranda
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="nav-link" href="{{ url('/visi-misi') }}">
                                                        Visi Misi
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="nav-link" href="{{ url('/berita') }}">
                                                        Berita
                                                    </a>
                                                </li>
                                                <li class="dropdown">
                                                    <a class="dropdown-item dropdown-toggle">
                                                        Data Pengelolaan Sampah
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        <li><a class="dropdown-item active" href="{{ url('/timbunan-sampah') }}">Timbunan Sampah</a></li>
                                                        <li><a class="dropdown-item" href="{{ url('/komposisi-sampah') }}">Komposisi Sampah</a></li>
                                                        <li><a class="dropdown-item" href="{{ url('/sumber-sampah') }}">Sumber Sampah</a></li>
                                                    </ul>
                                                </li>
                                                <li class="dropdown">
                                                    <a class="dropdown-item dropdown-toggle">
                                                        Fasilitas Pengelolaan Sampah
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        <li><a class="dropdown-item" href="{{ url('/bank-sampah') }}">Bank Sampah</a></li>
                                                        <li><a class="dropdown-item" href="{{ url('/rumah-kompos') }}">Rumah Kompos</a></li>
                                                        <li><a class="dropdown-item" href="{{ url('/tps3r') }}">TPS3R</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a class="nav-link" href="{{ url('/kontak') }}">
                                                        Hubungi Kami
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>

                                <button class="btn header-btn-collapse-nav ms-3 ms-sm-4" data-bs-toggle="collapse" data-bs-target=".header-nav-main nav">
                                    <i class="fas fa-bars"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-nav-features header-nav-features-no-border p-static">
                <div class="header-nav-feature header-nav-features-search header-nav-features-search-reveal header-nav-features-search-reveal-big-search header-nav-features-search-reveal-big-search-full">
                    <div class="container">
                        <div class="row h-100 d-flex">
                            <div class="col h-100 d-flex">
                                <form role="search" class="d-flex h-100 w-100" action="page-search-results.html" method="get">
                                    <div class="big-search-header input-group">
                                        <input class="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="Type and hit enter...">
                                        <a href="#" class="header-nav-features-search-hide-icon"><i class="fas fa-times header-nav-top-icon"></i></a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div role="main" class="main">

            <section class="page-header page-header-modern bg-primary custom-page-header">

                <div class="custom-svg-style-1 svg-fill-color-primary position-absolute top-0 left-50pct transform3dx-n50 h-100 z-index-0">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 400" xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" data-plugin-float-element-svg="true">
                        <circle id="svg_2" r="7.5" cy="539.5" cx="209.5" fill="#FFF" opacity="0.2" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.3, 'transition': true, 'transitionDuration': 1000, 'isInsideSVG': true}" />
                        <circle id="svg_3" r="12" cy="211" cx="268" fill="#FFF" opacity="0.2" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.4, 'transition': true, 'transitionDuration': 2000, 'isInsideSVG': true}" />
                        <circle id="svg_4" r="17" cy="144" cx="1864" fill="#FFF" opacity="0.2" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.6, 'transition': true, 'transitionDuration': 1000, 'isInsideSVG': true}" />
                        <circle id="svg_6" r="28.5" cy="326.74992" cx="327.75073" stroke-miterlimit="10" stroke-width="3" stroke="6CD499" fill="none" />
                        <circle opacity="0.2" stroke="#ffffff" id="svg_8" r="21.5" cy="340.25" cx="90.74976" fill="none" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.3, 'transition': true, 'transitionDuration': 1000, 'isInsideSVG': true}" />
                        <circle stroke="#ffffff" opacity="0.2" id="svg_9" r="14.625" cy="689.625" cx="128.87476" fill="none" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.5, 'transition': true, 'transitionDuration': 2000, 'isInsideSVG': true}" />
                    </svg>
                </div>

                <div class="container position-relative z-index-1 mt-4 h-100">
                    <div class="row h-100">
                        <div class="col align-self-end">
                            <div class="d-block">
                                <span class="d-block custom-stroke-text-effect-1 custom-big-text-2 font-weight-bold opacity-2">Timbunan Sampah</span>
                            </div>
                            <div class="d-block">
                                <h1 class="text-color-light font-weight-bold positive-ls-3 custom-big-text-1 line-height-1 mb-0">Timbunan Sampah</h1>
                            </div>
                            <ul class="breadcrumb breadcrumb-light d-block py-3 mb-5">
                                <li><a href="#">Home</a></li>
                                <li class="active">Timbunan Sampah</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </section>
            <div class="card mb-4">
                <!-- begin box -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm" style="height: 40vh !important">
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand">
                                    <div class=""></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink">
                                    <div class=""></div>
                                </div>
                            </div>
                            <div id="chart-pasien-rj" style="display: block; width: 1086px; height: 131px;" class="chartjs-render-monitor" width="1086" height="131"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <br><br>
                    <div class="col-12 mx-auto">
                        <!-- begin box FILTER-->

                        <!-- end box FILTER -->
                        <br>


                        <div class="card" style="margin: 0; padding-bottom: 0px;">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12 col-sm-3">
                                        <label class="mb-0" for="filter_tahun">Tahun</label>
                                        <select class="custom-select custom-select-sm select2" id="dd_tahun">
                                            <option value="ALL">[SEMUA Tahun]</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022" selected="">2022</option>
                                        </select>
                                    </div>

                                    <div class="col-12 col-sm-4">
                                        <label class="mb-0" for="filter_id_propinsi">Kelurahan</label>
                                        <select class="custom-select custom-select-sm select2" id="filter_id_propinsi">
                                            <option value="">Pilih 1 Dati2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--box-header-->
                        </div>

                        <div class="card">
                            <!-- begin box -->
                            <div class="card-body">
                                <div id="tabeldata_wrapper" class="dataTables_wrapper">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="csel DTl">
                                            </div>
                                            <div class="col-4 BTN">
                                            </div>
                                        </div><!-- end row-->

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="tabeldata_wrapper" class="dataTables_wrapper dt-bootstrap4 table-responsive-md">
                                                    <table class="table table-borderless">
                                                        <thead>
                                                            <tr class="text-center">
                                                                <th>Tahun</th>
                                                                <th>Kelurahan</th>
                                                                <th>RW</th>
                                                                <th>RT</th>
                                                                <th>Timbulan Sampah</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($timbunan as $item )

                                                            <tr class="text-center">
                                                                <td>{{$item->created_at->translatedFormat('Y')}}</td>
                                                                <td>{{$item->kelurahan->nama}}</td>
                                                                <td>{{$item->rw}}</td>
                                                                <td>{{$item->rt}}</td>
                                                                <td>{{$item->satuan_barang}} Kg</td>
                                                            </tr>
                                                            @endforeach

                                                        </tbody>
                                                    </table>

                                                </div> <!-- end -->
                                            </div><!-- end row -->
                                        </div>

                                    </div><!-- end datawrapper -->
                                </div><!-- end box body -->
                            </div><!-- end box -->
                        </div>
                    </div>

                </div>
                <footer id="footer" class="mt-0">
                    <div class="container py-5 my-4">
                        <div class="row gy-4">
                            <div class="col-lg-2 align-self-center mb-4 mb-lg-0 text-center">
                                <a href="index.html" class="text-decoration-none">
                                    <img src="img/sipsb.png" width="125" height="35" class="img-fluid" alt="Footer Logo" />
                                </a>
                            </div>
                            <div class="col-lg-3">
                                <h4 class="font-weight-bold text-color-dark">About Us</h4>
                                <p class="text-3-5">
                                <a class="dropdown-item" href="{{ url('/admin') }}"><strong class="d-block text-color-dark">Sistem Informasi Pengelolaan Sampah <br> Kecamatan Semarang Barat</strong></a>
                                    <!-- Gedung Manggala Wanabakti, <br>
                                    Blok IV Lantai 5.Jalan Gatot Subroto <br>
                                    Jakarta 10270, Indonesia<br><br> -->
                                    Phone: <a href="tel:+62 21 57902763" class="text-decorat
                                    ion-none">+62 21 57902763</a><br>
                                    <!-- Email: <a href="mailto:sipsn@menlhk.go.id" class="text-decoration-none">sipsn@menlhk.go.id</a> -->
                                </p>
                            </div>
                            <div class="col-lg-3">

                            </div>
                            <div class="col-lg-3">
                                <img src="img/logo_g3jks.png" width="125" height="35" class="img-fluid" alt="Logo G3JKS" />
                                <img src="img/logo_bs.png" width="125" height="35" class="img-fluid" alt="Logo BS" />
                                <img src="img/logo_ksp.png" width="125" height="35" class="img-fluid" alt="Logo KSP" />
                                <img src="img/logo_gnpsdr.png" width="125" height="35" class="img-fluid" alt="Logo GNPSDR" />
                            </div>
                        </div>
                    </div>
                    <div class="footer-copyright bg-color-dark-scale-2">
                        <div class="container py-2">
                            <div class="row py-4">
                                <div class="col-md-4 d-flex align-items-center justify-content-center justify-content-md-start mb-2 mb-lg-0">
                                    <p class="text-color-grey text-3-5">Sistem Informasi Pengelolaan Sampah Kecamatan Semarang Barat © 2023. All Rights Reserved.</p>
                                </div>
                                <div class="col-md-8 d-flex align-items-center justify-content-center justify-content-md-end mb-4 mb-lg-0">
                                    <ul class="footer-social-icons social-icons social-icons-clean social-icons-icon-light social-icons-big">
                                        <li class="social-icons-instagram"><a href="http://www.instagram.com/" target="_blank" title="Intagram"><i class="fab fa-instagram text-4"></i></a></li>
                                        <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter text-4"></i></a></li>
                                        <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f text-4"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>

            <!-- Vendor -->
            <script src="vendor/plugins/js/plugins.min.js"></script>

            <!-- Theme Base, Components and Settings -->
            <script src="js/theme.js"></script>

            <!-- Current Page Vendor and Views -->
            <script src="js/views/view.contact.js"></script>

            <!-- Theme Custom -->
            <script src="js/custom.js"></script>

            <!-- Theme Initialization Files -->
            <script src="js/theme.init.js"></script>
            <script src="dist/apexcharts.min.js"></script>
            @livewireScripts('scripts')
            <livewire:scripts />

</body>

@push('scripts')

<script>
    var chart = document.querySelector("#chart-pasien-rj")
    var options = {
        series: [{
            name: 'Jumlah',
            data: [44, 55, 57, 56, 61, 58, 63, 60, 66, 34, 45, 45]
        }],
        chart: {
            type: 'bar',
            height: 350,
            width: 1250,
        },
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '25%',
                endingShape: 'rounded'
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        },
        yaxis: {
            title: {
                text: 'Kg'
            }
        },
        fill: {
            opacity: 1
        },
        tooltip: {
            y: {
                formatter: function(val) {
                    return "Kg"
                }
            }
        }
    };

    var chart = new ApexCharts(document.querySelector("#chart-pasien-rj"), options);
    chart.render();

    Livewire.on('render_chart_kunjungan_pasien_rj', (series, label) => {
        pasien_rj(`#chart-pasien-rj`, series, label);
    });


</script>
@endpush

</html>

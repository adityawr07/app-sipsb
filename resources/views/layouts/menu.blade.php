<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="">SIPSB</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="">SIPSB</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="{{ request()->is(['/admin']) ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/admin') }}"><i class="fas fa-fire"></i> <span>Dashboard</span></a>
            </li>
            {{-- <li class="{{ request()->is(['transaksi/barang-masuk']) ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/transaksi/barang-masuk') }}"><i class="fas fa-sign-in-alt"></i> <span>Barang Masuk</span></a>
            </li>
            <li class="{{ request()->is(['transaksi/barang-keluar']) ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/transaksi/barang-keluar') }}"><i class="fas fa-sign-out-alt"></i> <span>Barang Keluar</span></a>
            </li> --}}
            @if (Auth::user()->hasRole('admin'))
                <li class="{{ request()->is(['admin/manajemen/data-barang']) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('admin/manajemen/data-barang') }}"><i class="fas fa-laptop"></i> <span>Data Barang</span></a>
                </li>
                <li class="menu-header">Manajemen Data</li>
                <li class="{{ request()->is(['admin/manajemen/data-berita']) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('admin/manajemen/data-berita') }}"><i class="fas fa-users"></i> <span>Berita</span></a>
                </li>
                <li class="{{ request()->is(['admin/manajemen/data-pegawai']) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('admin/manajemen/data-pegawai') }}"><i class="fas fa-users"></i> <span>Data User</span></a>
                </li>
                <li class="{{ request()->is(['admin/manajemen/data-pegawai']) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('admin/manajemen/pengelolaan-sampah') }}"><i class="fas fa-users"></i> <span>Pengelolaan Sampah</span></a>
                </li>

            @else

            @endif
            <li class="menu-header">Laporan</li>
            <li class="{{ request()->is(['laporan/barang-masuk']) ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/laporan/barang-masuk') }}"><i class="fas fa-print"></i> <span>Barang Masuk</span></a>
            </li>
            <li class="{{ request()->is(['laporan/barang-keluar']) ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/laporan/barang-keluar') }}"><i class="fas fa-print"></i> <span>Barang Keluar</span></a>
            </li>

        </ul>
    </aside>
</div>


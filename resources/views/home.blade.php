@extends('layouts.app')

@section('title')
SISTEM IMFORMASI SAMPAH KECAMATAN SEMARANG BARAT

@endsection

@section('content')

<div class="row">
    <div class="col-12 mb-4">
        <div class="hero bg-primary text-white">
            <div class="hero-inner">
                <h2>Welcome Back, {{ Auth()->user()->name }}!</h2>
                <p class="lead">SISTEM IMFORMASI SAMPAH KECAMATAN SEMARANG BARAT <span class="font-weight-bold">(SIPSB)</span>.</p>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_barang_keluar', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('no_transaksi');
            $table->dateTime('tanggal_transaksi');
            $table->string('keterangan')->nullable();
            $table->date('tanggal_pembayaran')->nullable();
            $table->decimal('jumlah_pembayaran', 20);
            $table->string('nama_pengirim')->nullable();
            $table->date('tanggal_pengiriman')->nullable();
            $table->string('nama_pelanggan');
            $table->string('kontak_pelanggan')->nullable();
            $table->string('alamat_pelanggan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_barang_keluar');
    }
};

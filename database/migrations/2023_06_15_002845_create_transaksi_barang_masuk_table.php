<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_barang_masuk', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('no_transaksi');
            $table->dateTime('tanggal_transaksi');
            $table->unsignedBigInteger('supplier_id');
            $table->string('keterangan')->nullable();
            $table->date('tanggal_pembayaran')->nullable();
            $table->decimal('jumlah_pembayaran', 20);
            $table->string('nama_penerima')->nullable();
            $table->date('tanggal_penerimaan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_barang_masuk');
    }
};

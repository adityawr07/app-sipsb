<?php

use App\Http\Controllers\BarangController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\DashboardController;
use App\Http\Livewire\DataPegawaiComponent;
use App\Http\Livewire\DataPresensiComponent;
use App\Http\Livewire\JamKerjaComponent;
use App\Http\Livewire\Laporan\BarangKeluarComponent as LaporanBarangKeluarComponent;
use App\Http\Livewire\Laporan\BarangMasukComponent as LaporanBarangMasukComponent;
use App\Http\Livewire\Manajemen\DataPengelolaanSampahComponent;
use App\Http\Livewire\PegawaiComponent;
use App\Http\Livewire\RiwayatPresensiComponent;
use App\Http\Livewire\Transaksi\BarangKeluarComponent;
use App\Http\Livewire\Transaksi\BarangMasukComponent;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware('role:admin|user')->group(function () {
    Route::get('/admin', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/pegawai', PegawaiComponent::class)->name('pegawai');
    Route::get('/barang/{barang}', [BarangController::class, 'getBarang']);
    Route::get('/list-barang', [BarangController::class, 'listBarang']);
    Route::get('/supplier/{supplier}', [BarangController::class, 'getSupplier']);

    Route::prefix('transaksi')->group(function () {
        Route::get('/barang-masuk', BarangMasukComponent::class)->name('transaksi-barang-masuk');
        Route::post('/barang-masuk', [TransaksiController::class, 'barangMasuk'])->name('transaksi-barang-masuk');
        Route::post('/barang-keluar', [TransaksiController::class, 'barangKeluar'])->name('transaksi-barang-masuk');
        Route::get('/barang-keluar', BarangKeluarComponent::class)->name('transaksi-barang-keluar');
    });

    Route::prefix('laporan')->group(function () {
        Route::get('barang-masuk', LaporanBarangMasukComponent::class)->name('laporan-barang-masuk');
        Route::get('barang-keluar', LaporanBarangKeluarComponent::class)->name('laporan-barang-keluar');
    });
});

Route::middleware('role:admin')->group(function () {
    Route::prefix('/admin/manajemen')->group(function () {
        Route::get('data-barang', \App\Http\Livewire\Manajemen\DataBarangComponent::class)->name('manajemen-data-barang');
        Route::get('data-supplier', \App\Http\Livewire\Manajemen\DataSupplierComponent::class)->name('manajemen-data-supplier');
        Route::get('data-pegawai', DataPegawaiComponent::class)->name('data-pegawai');
        Route::get('data-berita', \App\Http\Livewire\Manajemen\DataBerita::class)->name('data-berita');
        Route::get('pengelolaan-sampah', DataPengelolaanSampahComponent::class);
    });
});

// Route::prefix('dashboard')->group(function () {
    Route::get('/',[DashboardController::class, 'index']);
    Route::get('/berita',[DashboardController::class, 'berita']);
    Route::get('/visi-misi',[DashboardController::class, 'visimisi']);
    // Route::get('/timbunan-sampah',[DashboardController::class, 'timbunansampah']);
    Route::get('/timbunan-sampah',\App\Http\Livewire\Pengelolaan\TimbunanSampahComponent::class);
    Route::get('/komposisi-sampah',\App\Http\Livewire\Pengelolaan\KomposisiSampahComponent::class);
    Route::get('/sumber-sampah',\App\Http\Livewire\Pengelolaan\KomposisiSampahComponent::class);
    Route::get('/bank-sampah',[DashboardController::class, 'banksampah']);
    Route::get('/rumah-kompos',[DashboardController::class, 'rumahkompos']);
    Route::get('/tps3r',[DashboardController::class, 'tps3r']);
    Route::get('/kontak',[DashboardController::class, 'kontak']);

// });

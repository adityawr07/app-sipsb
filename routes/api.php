<?php

use App\Http\Controllers\TransaksiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/riwayat-transaksi-barang-masuk', [TransaksiController::class, 'riwayatTransaksiBarangMasuk']);
Route::delete('/delete-transaksi-barang-masuk', [TransaksiController::class, 'deleteTransaksiBarangMasuk']);
Route::get('/riwayat-transaksi-barang-keluar', [TransaksiController::class, 'riwayatTransaksiBarangKeluar']);
Route::delete('/delete-transaksi-barang-keluar', [TransaksiController::class, 'deleteTransaksiBarangKeluar']);
